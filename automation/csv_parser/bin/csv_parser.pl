#!/usr/bin/perl
#
##

##
## csv_parser tool - Joins csv files together and split based on criteria.
##               Also can be used as an advanced column modifier to rearrange or
##               modify columns.
##
##	Two modes of operation:
##		daemon - will stay running, polling a folder on a frequency and looking for filespec
##                       and/or files that have a specific age (minimum time a single file must be
##			 around for.  Once met, all files that match the filespec will be considered
##			 and examined.  Files read in are merged into a single dataset and then, if specified
##			 split based on criteria when written to the destination.
##		cli    - Will run once, given an inbound and outbound filename.  Will process the file based
##			 on config.
##
##		Both methods offer advanced rewriting of the csv file, including:
##		- reordering of columns
##		- search and replace of data per column
##		- search and replace based on a table in the config file.
##		- conversion of data types and reformatting of numbers and significant digits
##		- text formatting
##		- math functions
##		- table lookup of different formatting options based on a secondary key field.
##
#############################################################################################


use Time::Local;

# Figure out my name and what directory I'm running in.
my @whoami=split(/\//,$0);
my $whoami=pop(@whoami);
my $path=join("/", @whoami);
my $configfull;

$path="." if (!$path);

# Command line options to select config file and affect log levels.
# parse command line opts.
while ($arg = shift(@ARGV)) {

	if ($arg =~ /^-d$/) {
		$loglevel=3;
	}

	if ($arg =~ /^-i$/) {
		$inbound=shift(@ARGV);
		$runonce=1;
		$logstd=1;
	}

	if ($arg =~ /^-o$/) {
		$outbound=shift(@ARGV);
		$runonce=1;
		$logstd=1;
	}

	if ($arg =~ /^-v$/) {
		$loglevel=2;
	}

	if ($arg =~ /^-c$/) {
		$config=shift(@ARGV);
	}

	if ($arg =~ /^-C$/) {
		$configfull=shift(@ARGV);
	}

	if ($arg eq '-h' || $arg eq '--help') {
		help();
		exit 0;
	}

}

if (!$config && !$configfull) {
	help();
	exit 1;
}

# read in config file.
my %ini;
$configfull = $path . '/../conf/' . $config if (!$configfull);
my $ini = readini($configfull);
%ini=%{$ini} if $ini;

# define loglevel is specified 'debug mode'
$ini{'default'}{'loglevel'} = $loglevel if $loglevel;

writelog("INFO","INIT","Process started for $configfull.");


# if 'runonce', fill in some gaps that won't be in the ini file.
if ($runonce) {
    writelog("INFO","RUNONCE","Starting.");

	my (@inpath) = split(/\//,$inbound);
	my $infile = pop(@inpath);
	my $inpath = join('/', @inpath);
	$inpath = '.' if (!$inpath);

	if (-e $inbound) {
		#inbound file exists
		$ini{inbound}{dir}=$inpath;

		process_age(\%ini,$infile,\@files,$outbound);

	}

	exit 0
}


# Making sure the paths defined exist. Better now than error out later for something silly.
# this is done silently unless there is a real error.  -p won't whine if it already exists.
`mkdir -p "$path/clients"`;
my $dir = $ini{'inbound'}{'dir'};
`mkdir -p "$dir"`;
my $dir = $ini{'archive'}{'dir'};
`mkdir -p "$dir"`;
my $dir = $ini{'outbound'}{'dir'};
`mkdir -p "$dir"`;

#
# Main loop.  This controls looking for files and determining if any files are old enough to process.
#
while (1) {

	my @files = get_files($ini{'inbound'}{'dir'},$ini{'inbound'}{'filespec'});

	if ($#files >= 0 ) {

		my $oldest = find_oldest($ini{'inbound'}{'dir'},\@files);

		if (age($ini{'inbound'}{'dir'},$oldest) > $ini{'default'}{'age'}) {

			writelog('INFO','MAIN',"Found file older than " . $ini{'default'}{'age'} . ", $oldest.");

			process_age(\%ini,$oldest,\@files);

		} else {
			writelog('DEBUG','MAIN',"Sleeping " . $ini{'default'}{'sleep'} . " second(s).");
			sleep $ini{'default'}{'sleep'};
		}
		
	} else {

		if ($ini{'default'}{'runonce'} =~ /y/i) {
			writelog('INFO','MAIN',"All files processed, exiting.");
			exit;
		}
		writelog('DEBUG','MAIN',"Sleeping " . $ini{'default'}{'sleep'} . " second(s).");
		sleep $ini{'default'}{'sleep'};
	}

}

writelog('ERROR','MAIN',"Main Loop made impossible exit.  Shutting down.");
exit 1;


######
##  Functions
######

#
# process_age - will take a list of files and look for matches based on results from get_key.
#                
sub process_age {

	my ($ini,$primary,$files,$outfile2) = @_;
	my %ini = %{$ini} if $ini;
	my @files = @{$files} if $files;
	my @outfile;
	my @outheader;
	my @processed;

	writelog('INFO','PROCESS',"Processing $primary, looking for matches.");

	# Getting unique key...

	my $pri_file = $ini{'inbound'}{'dir'} . "/" . $primary;
	my $outfile = frename($primary,$ini{'default'}{'outbound'});
	my $out_file = $ini{'outbound'}{'dir'} . "/" . $outfile;
	$ini{'outbound'}{'delimiter'} = $ini{'default'}{'delimiter'} if (!$ini{'outbound'}{'delimiter'});

	# outbound file was specified on the command line.
	$out_file = $outfile2 if ($outfile2);

	my $pri_key = get_key($pri_file,$ini{'default'}{'delimiter'},$ini{'default'}{'key'},$ini{'default'}{'header'});

	push @outheader, read_header($pri_file,$ini{'default'}{'header'});
	push @outfile, read_file($pri_file,$ini{'default'}{'header'},$ini{'default'}{'format'},$ini{'default'}{'delimiter'});

	if ($ini{'default'}{'age'} > 0) {

		foreach $file (@files) {

			if ($file ne $primary) {

				$sib_file = $ini{'inbound'}{'dir'} . "/" . $file;

				my $key = get_key($sib_file,$ini{'default'}{'delimiter'},$ini{'default'}{'key'},$ini{'default'}{'header'});

				if ($key eq $pri_key) {

					writelog('INFO','PROCESS',"Found sibling, $file.  Processing.");

					push @outfile, read_file($sib_file,$ini{'default'}{'header'},$ini{'default'}{'format'},$ini{'default'}{'delimiter'});
					push @processed, $file;
				}
			}
		}
	} else {
		writelog('DEBUG','PROCESS',"Processing disabled, age = 0");
	}

	write_file($out_file,\@outheader,\@outfile,\%ini,$primary);

	push @processed, $primary;

	archive($ini,\@processed) if ($ini{'archive'});

}

#
# Straight-forward open file and write data to it from an array.  One element per line.
#
sub write_file {
	my ($file,$header,$data,$ini,$primary) = @_;
	my @data = @{$data} if $data;
	my @header = @{$header} if $header;
	my %ini = %{$ini} if $ini;
	my %keys;
	my $line;

	if ($ini{'default'}{'breakfile'} !~ /y/i) {
		if (open (FILE, ">$file")) {

			if ($ini{'default'}{'outheader'} !~ /n/i) {
				foreach $line (@header) {
					print FILE $line . "\n" if $line;
				}
			} else {
				writelog('INFO','WRITE_FILE',"Output of headers ommited.");
			}
				
			foreach $line (@data) {
				print FILE $line . "\n" if $line;
			}
			
			close FILE;

			writelog('INFO','WRITE_FILE',"File $file written.");
		} else {
			writelog('ERROR','WRITE_FILE',"Can't open $file for writing, exiting.");
			exit 4;
		}
	} else {
		writelog('INFO','WRITE_FILE',"File Breaking configured.");
		# gather keys
		foreach $line (@data) {
			my $mykey = get_key_line($line,$ini{'default'}{'delimiter'},$ini{'outbound'}{'breakcols'},$ini{'outbound'}{'breakdelimiter'});
			push @{$keys{$mykey}}, $line;
		}

		foreach $key (sort keys %keys) {
			my $file = $ini{'outbound'}{'dir'} . '/' . frename($primary,$ini{'outbound'}{'filespec'});
			$file =~ s/:key:/$key/gi;
			writelog('INFO','WRITE_FILE',"Key [$key] found, writing to file [$file].");

			if (open (FILE, ">$file")) {

				if ($ini{'default'}{'outheader'} !~ /n/i) {
					foreach $line (@header) {
						print FILE $line . "\n" if $line;
					}
				} else {
					writelog('INFO','WRITE_FILE',"Output of headers ommited.");
				}
				foreach $line (@{$keys{$key}}) {
					print FILE $line . "\n" if $line;
				}
			}
			close FILE;
		}
	}
}

#
# archive - move a file into the archive directory.  Will compress if configured.
#
sub archive {

	my ($ini,$files) = @_;

	my @files = @{$files} if $files;
	my %ini = %{$ini} if $ini;

	foreach $file (@files) {

		my $renfile = frename($file,$ini{'archive'}{'filespec'});
		if (!rename($ini{'inbound'}{'dir'} . '/' . $file, $ini{'archive'}{'dir'} . '/' . $renfile)) {
			writelog('ERROR','ARCHIVE',"Can't archive file $file. Dumping processed list and exiting.");
			foreach $f1 (@files) {
				writelog('INFO','ARCHIVE',"Processed file to be archived: $f1");
			}

			exit 3;
		}
		if ($ini{'archive'}{'compress'} =~ /y/i) {
			writelog('INFO','ARCHIVE',"Compressing $renfile.");
			my $cmd = "gzip -9vf \"" .  $ini{'archive'}{'dir'} . '/' . $renfile . "\"";
			writelog('DEBUG','ARCHIVE',"Compress CMD: $cmd");
			my $out = `$cmd`;
			writelog('DEBUG','ARCHIVE',"Compress OUT: $out");
		}
	}
}

#
# get_key - will take a file and determine a key based on a list of columns
#
# Note, this will ignore the header if configured and will generate the key based on a single line.  It
# is assumed, according to spec that all lines will have the same key result.
#
sub get_key {

	my ($file,$delim,$cols,$header) = @_;
	my @cols = split(/,/,$cols);

	writelog('DEBUG','GET_KEY',"Requested get_key on $file.");

	open (FILE, $file) || writelog ('WARN','GET_KEY', "Can't open file $file for reading.");

	if ($header > 0) {
		my $h;
		for ($h = 0; $h < $header; $h++) {
			my $line = <FILE>;
		}
	}

        my $line = <FILE>;
        chomp $line;

	writelog('DEBUG','GET_KEY',"Line:  [$line].");
	writelog('DEBUG','GET_KEY',"Delim: [$delim].");
	writelog('DEBUG','GET_KEY',"Cols:  [$cols].");

	my @line = split(/\s*$delim\s*/, $line);

	my $key;

	foreach $col (@cols) {
		writelog('DEBUG','GET_KEY',"Adding value [" . $line[$col - 1] . "] from Col [$col].");
		$key .= $line[$col - 1] . $delim;
	}
	
	close FILE;

	if ($key eq '') {
		writelog('WARN','GET_KEY',"No key found, possible incomplete file?");
	}

	return $key;

}

sub get_key_line {

	my ($line,$delim,$cols,$keydelim) = @_;
	my @cols = split(/,/,$cols);
	my @key;

	writelog('DEBUG','GET_KEY_LINE',"Requested get_key_line.");

        chomp $line;

	writelog('DEBUG','GET_KEY_LINE',"Line:  [$line].");
	writelog('DEBUG','GET_KEY_LINE',"Delim: [$delim].");
	writelog('DEBUG','GET_KEY_LINE',"Cols:  [$cols].");

	my @line = split(/\s*$delim\s*/, $line);

	my $key;

	foreach $col (@cols) {
		writelog('DEBUG','GET_KEY_LINE',"Adding value [" . $line[$col - 1] . "] from Col [$col].");
		push @key, $line[$col - 1];
	}
	$key = join($keydelim,@key);
	
	if ($key eq '') {
		writelog('WARN','GET_KEY_LINE',"No key found, possible incomplete line?");
	}

	return $key;
}

#
# read_header - will return the header in an array, one line per element.
#
sub read_header {

	my ($file,$header) = @_;
	my @header;

	open (FILE, $file) || writelog ('WARN','GET_KEY', "Can't open file $file for reading.");

	if ($header > 0) {
		my $h;
		for ($h = 0; $h < $header; $h++) {
			my $line = <FILE>;
			chomp $line;
			push @header, $line;
		}
	} 

	close FILE;

	return @header;

}

#
# read_file - will read a file (sans header) and return the result in an array, one line per element.
#             it will also process any column modifications at this time.
#
sub read_file {
	
        my ($file,$header,$format,$delim) = @_;
	my @outfile;
	my @format;

        writelog ('DEBUG','READ_FILE', "Got File[$file] header[$header] format[$format], delim[$delim]");
        open (FILE, $file) || writelog ('WARN','READ_FILE', "Can't open file $file for reading.");

	if ($header > 0) {
		my $h;
		for ($h = 0; $h < $header; $h++) {
			my $line = <FILE>;
		}
	}
	
	#if ($format) {
		#@format = split(/\|/,$format);
	#}

        while (my $l = <FILE>) {
                chomp $l;
		$l =~ s///g;

		if ($format) {
			my @l = split($delim,$l);
			my @l2;

			my $item;
			my @format = split(/\|/,$format);
		
			foreach $item (@format) {

				if ($item =~ /"/) {
					$item =~ s/"//g;
					my $val = detoken($item,$file,\@l);
					push @l2, $val;
				} else {
					my ($i,$mod) = split(/:/,$item);
					if ($mod) {
						$v = modifyfield($mod,$l[$i-1]);
						push @l2, $v;
					} else {
						writelog ('DEBUG','READ_FILE', "Don't modify field $i.");
						push @l2, $l[$i - 1];
					}
				}
			}
			$l = join($delim,@l2);
		    writelog ('DEBUG','READ_FILE', "FIN[$l]");
		}
                push @outfile, $l;
        }

	close FILE;

	return @outfile;

}


sub modifyfield {

	my ($modifier,$v) = @_;

	my ($mod,@opts) = split(/,/, $modifier);
	


	if ($mod =~ /t/i) {
		writelog ('DEBUG','READ_FILE', "Modify field $i to remove whitespace.");
		$v =~ s/\s*//g;
	}
	if ($mod =~ /_/i) {
		writelog ('DEBUG','READ_FILE', "Modify field $i to change spaces to _'s.");
		$v =~ s/ /_/g;
	}
	if ($mod =~ /l/i) {
		writelog ('DEBUG','READ_FILE', "Modify field $i to lowercase.");
		$v =~ tr/A-Z/a-z/;
	}
	if ($mod =~ /u/i) {
		writelog ('DEBUG','READ_FILE', "Modify field $i to uppercase.");
		$v =~ tr/a-z/A-Z/;
	}

	if ($mod =~ /x/i) {
		writelog ('DEBUG','READ_FILE', "Modify field $i search and replace.");

		my ($this,$that,$case,$every) = @opts;

		$this = detoken($this);
		$that = detoken($that);

		if ($every == 1){
			if ($case == 1){
				$v =~ s/$this/$that/g;
			} else {
				$v =~ s/$this/$that/gi;
			}
		} else {
			if ($case == 1){
				$v =~ s/$this/$that/;
			} else {
				$v =~ s/$this/$that/i;
			}
		} e

	}

        if ($mod =~ /!/) {
                writelog ('DEBUG','TABLEMODIFY', "Modify field $i using a table lookup key");
                my ($table) = @opts;
                $table =~ tr/A-Z/a-z/;
                foreach $val (sort keys %{$ini{$table}}) {
                        writelog ('DEBUG','TABLEMODIFY', "Entry $val [".$ini{$table}{$val}."]");
                        my ($col,$regex,@change) = split(/\//,$ini{$table}{$val});
                        $regex = detoken($regex,'',$list);
                        if ($list[$col-1] =~ /$regex/) {
                                my $change = join("/", @change);
                                my @opts = split(/:/, $change);
                                foreach $opt (@opts) {
                                        writelog ('DEBUG','TABLEMODIFY', "Modify [$v] with [$opt]");
                                        $v = modifyfield($opt,$v,\@list);
                                        writelog ('DEBUG','TABLEMODIFY', "Result [$v]");
                                }
                        }
                }
        }

        if ($mod =~ /M/) {
                writelog ('DEBUG','MODIFYFIELD', "Multiply field using $i table lookup forced (case sensative).");

                my ($table,$col,$pos1,$len1,$pos2,$len2,$app,$strl) = @opts;
                $table =~ tr/A-Z/a-z/;
                my $mult = table_lookup($table,$v,$pos1,$len1,$pos2,$len2,1,$app,$str,$v);
                $mult = 1 if (!$mult);
                $v = $list[$col-1] * $mult;
        }

        if ($mod =~ /m/) {
                writelog ('DEBUG','MODIFYFIELD', "Multiply field using $i table lookup forced.");

                my ($table,$col,$pos1,$len1,$pos2,$len2,$app,$str) = @opts;
                my $v2 = $v;
                $v2 =~ tr/A-Z/a-z/;
                $table =~ tr/A-Z/a-z/;
                my $mult = table_lookup($table,$v,$pos1,$len1,$pos2,$len2,1,$app,$str,$v);
                $mult = 1 if (!$mult);
                $v = $list[$col-1] * $mult;
        }

        if ($mod =~ /R/) {
                writelog ('DEBUG','READ_FILE', "Modify field $i table lookup forced (case sensative).");

                my ($table,$pos1,$len1,$pos2,$len2,$app,$str) = @opts;
                $table =~ tr/A-Z/a-z/;
                $v = table_lookup($table,$v,$pos1,$len1,$pos2,$len2,1,$app,$str,$v);
        }

        if ($mod =~ /r/) {
                writelog ('DEBUG','READ_FILE', "Modify field $i table lookup forced.");

                my ($table,$pos1,$len1,$pos2,$len2,$app,$str) = @opts;
                my $v2 = $v;
                $v2 =~ tr/A-Z/a-z/;
                $table =~ tr/A-Z/a-z/;
                $v = table_lookup($table,$v2,$pos1,$len1,$pos2,$len2,1,$app,$str,$v);
        }

        if ($mod =~ /S/) {
                writelog ('DEBUG','READ_FILE', "Modify field $i table lookup (case sensative).");

                my ($table,$pos1,$len1,$pos2,$len2,$app,$str) = @opts;
                $table =~ tr/A-Z/a-z/;
                $v = table_lookup($table,$v,$pos1,$len1,$pos2,$len2,0,$app,$str,$v);
        }

        if ($mod =~ /s/) {
                writelog ('DEBUG','READ_FILE', "Modify field $i table lookup.");

                my ($table,$pos1,$len1,$pos2,$len2,$app,$str) = @opts;
                my $v2 = $v;
                $v2 =~ tr/A-Z/a-z/;
                $table =~ tr/A-Z/a-z/;
                $v = table_lookup($table,$v2,$pos1,$len1,$pos2,$len2,0,$app,$str,$v);
        }

	if ($mod =~ /t/i || $mod =~ /_/i || $mod =~ /l/i || $mod =~ /u/i || $mod =~ /c/i) {
		if ($#opts >= 0) {
			my ($pos,$len) = @opts;
			if ($len == 0) {
				$v = substr($v,$pos);
			} else {
				$v = substr($v,$pos,$len)
			}
		}
	}
			
	if ($mod =~ /d(\d)/i || $mod =~ /d/i) {
		writelog ('DEBUG','READ_FILE', "Modify field $i convert from fraction to decimal [$v].");
		my $places;
		if ($#opts >= 0) {
			$places=$opts[0];
		} else {
			$places = $1;
		}
		if ($v =~ /\//) {
			my ($a,$b) = split (/ +/,$v);
			my ($c,$d) = split (/\//,$b);
			writelog ('DEBUG','READ_FILE', "A[$a] B[$b] C[$c] D[$d].");
			my $e = $c / $d + $a;
			if ($places) {
				$v = sprintf("%.".$places."f",$e);
			} else {
				$v = $e;
			}
		} else {
			writelog ('DEBUG','READ_FILE', "field $i was not a fraction, no change.");
			$v = sprintf("%.".$places."f",$v);
		}
	}

	return $v;

}

#
# table lookup, using options for clipping and appending
#

sub table_lookup {
        my ($table,$key,$keypos,$keylen,$valpos,$vallen,$forcereplace,$append,$appstr,$orgkey) =@_;

        $default = $ini{$table}{''};
        my $result;
        my $source;

        if ($ini{$table}) {
                if ($keypos >= 0) {
                        if ($keylen == 0) {
                                $source = substr($key,$$keypos);
                        } else {
                                $source = substr($key,$keypos,$keylen);
                        }
                } else {
                        $source = $key;
                }
                $found = $ini{$table}{$source};
                # forcereplace will force the replace even if nothing found.
                if ($found || $forcereplace == 1) {
                        $found = $default if (!$ini{$table}{$source} && $forcereplace == 1);
                        if ($valpos >= 0) {
                                if ($vallen == 0 && $vallen ne "0") {
                                        $result = substr($found,$valpos);
                                } else {
                                        $result = substr($found,$valpos,$vallen);
                                }
                        } else {
                                $result = $found;
                        }
                        writelog ('DEBUG','TABLE_LOOKUP', " Found [".$orgkey."] in table [".$table."] returned [".$result."].");
                } else {
                        $result = $orgkey;
                        writelog ('DEBUG','TABLE_LOOKUP', " Didn't Find [".$orgkey."] in table [".$table."].");
                }

                if ($ini{$table}{$source}){
                        if ($append == 1) {
                                $result = $result . $appstr;
                        }
                } else {
                        if ($append == 2) {
                                $result = $result . $appstr;
                        }
                }

                writelog ('DEBUG','TABLE_LOOKUP', "Search Lookup for [".$source."][".$orgkey."] in table [".$table."] returned [".$result."] [".$forcereplace."].");
                $result = detoken($result);
        } else {
                writelog ('WARN','TABLE_LOOKUP', "Requested search lookup but table [".$table."] not found.");
        }

        return $result;
}



#
# age - will return the age of a file in seconds from 'now'.
#
sub age {

	my ($dir,$file) = @_;

	my $now = timelocal(localtime(time));
	($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat($dir . "/" . $file);
	my $age = $now - $mtime;

	writelog('DEBUG','AGE',"Age for $file is $age");

	return $age;

}

#
# find_oldest - Will locate the oldest file in a list and return it's name.
#
sub find_oldest {

	my ($dir,$files) = @_;

	my @files = @{$files} if $files;

	my $now = timelocal(localtime(time));
	my $old_age;
	my $old_file;

	foreach $file (@files) {

		($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat($dir . "/" . $file);
		my $age = $now - $mtime;

		if ($age > $old_age) {
			$old_age = $age;
			$old_file = $file;
		}
	}

	writelog('DEBUG','OLDEST',"Found oldest file: $old_file with age $old_age.");

	return $old_file;
}

#
# help - display usage info.
#
sub help {
	print "\n";
	print "Usage: csv_parser.pl -c configfile.ini\n";
	print "\n";
	print "  -c      Specify the config file to process. (required)\n";
	print "          Config files are located in the ../conf directory.  Don't specify path.\n";
	print "  -C      Specify config file with the full path.\n";
	print "  -i      Specify the input file (full path). Disables inbound setting in config.`\n";
	print "  -o      Specify the output file (full path). Disables outbound in config.\n";
	print "  -v      Verbose level logging.\n";
	print "  -d      Debug level logging, includes verbose.\n";
	print "\n";
	print "  Notes:\n";
	print "   - options -i and -o disable running with in a loop, it only runs once.\n";
	print "\n";
}

#
# detoken - will change text by replacing <> tokens with information.
#
sub detoken {

        my ($file,$fname,$list) =@_;

	my @list = @{$list} if $list;

        writelog("DEBUG","DETOKEN","Starting detoken: [$file][$fname]");

        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
        $year += 1900;
        $mon ++;
        $sec  = sprintf("%02d", $sec);
        $min  = sprintf("%02d", $min);
        $hour = sprintf("%02d", $hour);
        $mday = sprintf("%02d", $mday);
        $mon  = sprintf("%02d", $mon);
        $year = sprintf("%0004d", $year);
        $yr = $year;
        $yr =~ s/^..//g;

        $file =~ s/<sec>/$sec/g;
        $file =~ s/<min>/$min/g;
        $file =~ s/<hour>/$hour/g;
        $file =~ s/<mday>/$mday/g;
        $file =~ s/<day>/$mday/g;
        $file =~ s/<mon>/$mon/g;
        $file =~ s/<year>/$year/g;
        $file =~ s/<yr>/$yr/g;
        $file =~ s/<slash>/\//g;
        $file =~ s/<bslash>/\\/g;
        $file =~ s/<pipe>/\|/g;
        $file =~ s/<comma>/,/g;
        $file =~ s/<file>/$fname/g;
        $file =~ s/<br>/\n/g;

	my $clean;
	@token=split(/</,$file);

	$clean = shift @token;

	foreach $token (@token) {

		my @detoken = split(/>/,$token);

		my $token2 = shift @detoken;
		$end = join('>',@detoken);

		my @opts = split(/:/,$token2);

		my $tok = shift(@opts);
		my $val;

		if ($tok =~/\d+/) {
			writelog("DEBUG","DETOKEN","Field [$tok][".$list[$tok-1]."] with options [".join(':',@opts)."]");
			$tok = $list[$tok-1];

			if ($#opts >= 0) {
				# options given
				foreach $opt (@opts) {
					# process each option
					writelog("DEBUG","DETOKEN","Before Modify [$opt][$tok]");
					$tok = modifyfield($opt,$tok);
					writelog("DEBUG","DETOKEN","After Modify [$tok]");
				}
			}
		} else {
			$tok = '';
		}

		$clean .= $tok . $end;
	}

        writelog("DEBUG","DETOKEN","Return: [$clean]");
        return $clean;
}

# readini - Generic read ini into hash routine.
#
#  It will parse in standard [section] based ini files.  Items before the first
#  'section' will be in 'default'.  Sections are case insensative and dropped to
#  lowercase apon detection.  Keys are also dropped to lowercase.
sub readini {

        my ($ini) = @_;

        $section='default';
        writelog("DEBUG","READINI","Reading in [$ini]");
        open(INI, "< $ini");
        while ($l=<INI>) {
                chomp $l;
                if ($l =~ /\[(.+)\]/) {
                        $section=$1;
                        $section=~ tr/A-Z/a-z/;
                } else {
                        ($var,@vl)=split(/=/, $l);
                        $var=~ tr/A-Z/a-z/;
                        if (@vl) {
                                $ini{$section}{$var}=join("=",@vl);
                        }
                }
        }
        close INI;

        foreach $section (sort keys %ini) {
                foreach $key (sort keys %{$ini{$section}}) {
                        writelog("DEBUG","READINI","INI[$section][$key]=[".$ini{$section}{$key}."]");
                }
        }

        return \%ini;
}

#
# writelog - This is my all purpose log routine.  I modified it to use settings from the INI file.
# $stat is usually ERROR, DEBUG or INFO.  $group can be anything, and $msg is open for
# whatever.
#
sub writelog {

        my ($stat,$group,$msg) = @_;

                my %level;
                $level{'DEBUG'}=3;
                $level{'INFO'}=2;
                $level{'WARN'}=1;
                $level{'ERROR'}=0;

        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
        $year += 1900;
        $mon++;

                my $level = $ini{'default'}{'loglevel'};

                if ($level >= $level{$stat}) {
                        if ($ini{'default'}{'logprefix'}) {
                                my $log = $ini{'default'}{logdir} . "/" . $ini{'default'}{'logprefix'};
                                my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
                                $year += 1900;
                                $mon ++;
                                $log .= sprintf("%0004d%02d%02d.log",$year,$mon,$mday);
                                chomp $msg;
                                open (LOG, ">> $log");
                                print LOG sprintf("%0004d%02d%02d-%02d%02d%02d: [%-7s] %s - %s\n", $year,$mon,$mday,$hour,$min,$sec,$stat,$group,$msg);
                                close LOG;
                        } else {
                                print $out sprintf("%0004d%02d%02d-%02d%02d%02d: [%-7s] %s - %s\n", $year,$mon,$mday,$hour,$min,$sec,$stat,$group,$msg);
                        }
                print sprintf("%0004d%02d%02d-%02d%02d%02d: [%-7s] %s - %s\n", $year,$mon,$mday,$hour,$min,$sec,$stat,$group,$msg);
        }
}

#
# frename - rename a file based on a regex/token spec.
#
sub frename {
        my ($file,$spec) = @_;

        my $ofile = $file;

        @specs = split(/\|/,$spec);

        foreach $regex (@specs) {
                my ($z,$opt1,$opt2,$arg) = split(/\//,$regex);
                $file =~ s/$opt1/$opt2/ if $opt1;
        }

        $outfile = detoken($file);

        writelog("DEBUG","RENAME","Rename $ofile -> $file [$spec]");
        return $outfile;
}


#
# get_files - This function will accept a directory and a filespec (* and ? are valid) and return all
# the files that match in an array, one per element.  Note that directory is not prefixed.
#
sub get_files {

        my ($dir,$fspec) = @_;

                $fspec =~ s/\./\\\./g;
                $fspec =~ s/\*/.*/g;
                $fspec =~ s/\?/./g;
                $fspec =~ s/^/^/g;
                $fspec =~ s/$/\$/g;

                opendir(DIR,$dir);
                my @files = grep { /$fspec/ && -f "$dir/$_" } readdir(DIR);
                closedir(DIR);

                my $files=$#files+1;

                writelog("DEBUG","GET_FILES","Looking in [$dir] for [$fspec].");
                writelog("DEBUG","GET_FILES","Found: $files file(s).");
        return @files;
}

