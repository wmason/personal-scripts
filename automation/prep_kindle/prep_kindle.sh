#!/bin/bash
#
# on Kindle press <shift><enter> to get search.
# then type:
#    ;debugOn<enter>
# then type:
#    ~usbNetwork<enter>
#(all case sensitive)
#

MYPATH="`dirname $0`"

SSH_KEY="`cat ~/.ssh/id_rsa.pub`"
KINDLE_IP=192.168.2.2
SSH_AUTH_FILE=/mnt/us/usbnet/etc/authorized_keys
SOURCE="${MYPATH}/core"
DEST="/mnt/us/contag"
DATA="${MYPATH}/data"

if [[ "$OS" == "Windows_NT" ]]; then
	$SYSTEMROOT/system32/ping -n 1 $KINDLE_IP 2>&1 >/dev/null
else
	ping -c 1 $KINDLE_IP 2>&1 >/dev/null
fi
ST=$?

if [[ $ST -gt 0 ]]; then
	echo ""
	echo "Kindle not found on $KINDLE_IP"
	exit 1
fi

DATADIR=""
ENABLE=""

while [[ -n "$1" ]]; do

	OPT="$1"
	shift

	if [[ "${OPT}" == "--data" ]]; then
		DATADIR="${DATA}/$1"
		shift

		if [[ ! -d "${DATADIR}" ]]; then
			echo ""
			echo "ERROR: ${DATADIR} does not exist."
			exit 5
		fi
	elif [[ "${OPT}" == "--enable" ]]; then
		ENABLE='y'
	elif [[ "${OPT}" == "--stopcron" ]]; then
		ENABLE='dc'
	elif [[ "${OPT}" == "--disable" ]]; then
		ENABLE='n'
	elif [[ "${OPT}" == "-h" || "${OPT}" == "--help" ]]; then
		echo ""
		echo " Usage:  prep_kindle.sh [--data {datadir}] [--enable|--disable] "
		echo ""
		echo "  {datadir}    Locatied in ../kindle, created by export_kindle.php."
		echo "  --enable     Enables Contag Mode, adds cron."
		echo "  --disable    Disables Contag Mode, restores system cron."
		echo "  --stopcron   Disables crontab, resets to normal system cron."
		echo ""
		exit 0 
	fi
done

ssh-keygen -R ${KINDLE_IP}
VERSION="`ssh -o"StrictHostKeyChecking=no" -o"BatchMode=yes" root@${KINDLE_IP} "cat /etc/prettyversion.txt" 2>/dev/null`"
ST=$?

if [[ $ST -gt 0 ]]; then
	# need ssh key installed
	echo ""
	echo "- Install ssh key.  Enter kindle root password where needed."
	ssh root@$KINDLE_IP "mount -o remount,rw /mnt/base-us; echo '${SSH_KEY}' >> ${SSH_AUTH_FILE}" 2>/dev/null
	ST=$?

	if [[ $ST -gt 0 ]]; then
		echo ""
		echo "Failed to install key."
		exit 2
	fi
	VERSION="`ssh -o"StrictHostKeyChecking=no" -o"BatchMode=yes" root@${KINDLE_IP} "cat /etc/prettyversion.txt" | tail -1`"
else
	ssh root@$KINDLE_IP "mount -o remount,rw /mnt/base-us" 2>/dev/null
fi

echo "Kindle Version: ${VERSION}"

echo "- Copy / refresh scripts"

ssh root@$KINDLE_IP "rm -rf ${DEST} " 2>/dev/null
ssh root@$KINDLE_IP "mkdir -p ${DEST}/data ${DEST}/../save" 2>/dev/null

scp -rp ${SOURCE}/. root@$KINDLE_IP:${DEST}

if [[ -n "$DATADIR" ]]; then
	scp -rp ${DATADIR}/. root@$KINDLE_IP:${DEST}/data
	ST=$?
	if [[ $ST -gt 0 ]]; then
		echo "ERROR: Failed to copy data from ${DATADIR}."
		exit 3
	fi
fi
	
if [[ "$ENABLE" == "y" ]]; then
	echo "- Enabling Contag Mode"
	ssh root@$KINDLE_IP "(cd ${DEST}; ./contag-install.sh)" #2>/dev/null
elif [[ "$ENABLE" == "dc" ]]; then
	ssh root@$KINDLE_IP "(cd ${DEST}; ./contag-stopcron.sh)" #2>/dev/null
elif [[ "$ENABLE" == "n" ]]; then
	echo "- Disabling Contag Mode, Renabled Kindle functions."
	ssh root@$KINDLE_IP "(cd ${DEST}; ./contag-restore.sh)" 2>/dev/null
fi

DSTAMP="`date +%m%d%H%M%Y.%S`"
ssh root@$KINDLE_IP "/bin/date -s ${DSTAMP}"

echo "- Securing Device."
ssh root@$KINDLE_IP "mount -o remount,ro /mnt/base-us" 2>/dev/null
