# Personal Scripts #

This is a collection of scripts created for my personal use or for unix automation.  This project is intended as an example of my work and not as a real continuous project.


# Automation #

Various scripts for automating arduous tasks.

Dir: automation

## prep_kindle ## 

Script: prep_kindle.sh

Description:  Written to take a remote linux host (in this case a hacked Amazon Kindle III) via ssh and prepare it for 'our' framework.  This shows off various methods to automate setting up a new host: installing local ssh key, querying host for information, running scripts to perform specific functions.  It will not be useful without the 'expected' environment, but shows what can be done with some creative ssh access.

## CSV Parser ##

Script: csv_parser.pl -c {config file} [-i {input file} -o {output file}]

Description:  A script to rewrite a csv file based on criteria.  See [documentation](https://bitbucket.org/wmason/personal-scripts/wiki/csv_parser)

# Notifications #

This category contains scripts used for sending messages and other bits of text to mobile devices and other desktops.  Can be incorporated into other scripts to give them additional functions.

Dir: notification

## PushOver ##

Script: po [{app}] {message}

Description:  Used to send messages via the [PushOver](https://pushover.net/api) API.  The API allows for multiple application tokens to defined.  This script allows you to add them to it and uses the first word of the message as the defined 'app'.  

## PushBullet ##

Script: pb {message}

Description:  Used to send messages via the [PushBullet](http://docs.pushbullet.com/) API.


# Tools #

A collection of tools used to simplify common tasks.

Dir: tools

## rename2 ##

Script: rename2

Description:  Renames files in the current directory using VI.  It was clear at one point I was quicker to make bulk edits (search/replace/repeat edits) in VI. 
 Not recommended for beginner VI users.  Source can be changed to use editor of choice.  'renfile' needs to be in the path for this to function.  Renfile is written in perl to simplify renaming files with special characters that might give shell scripting trouble.

## Port Check ##

Script port.sh

Description:  Script for testing if a host/port is accessible across a complex network.  Config file based, a single config file can contain multiple source/destination hosts.  Only source hosts that match the current hostname will be tested.  This allows the same script and config file to be synced throughout your environment.

## Listener ##

Script: listener.pl

Description:  Perl script to start listening to a specific port and reply interactively to a telnet to the same port.  Useful to verify if a port is available across the network before the actual service has been installed.  Script is lightweight and can be used on most unix variants.

# Image Sorting #

Scripts to help manage images based on certain criteria.

Dir: image_sorting

## mvimg ##

Script: mvimg

Description:  Searches the current directory for a selection of file extensions.  Using a variery of methods (filename/exif), attempt to determine when an image was taken.  Once determined, move the image into a formatted dated directory in the $TARGET.  In the event of a duplicate file in the destination, they are compared and if different, will modify the filename to append a current timestamp.