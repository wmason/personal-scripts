#!/usr/bin/perl -w
# test.pl
#----------------

use strict;
use Socket;

# initialize host and port
my $host = shift;
my $port = shift;

my($name, $aliases, $type, $len, $server) = gethostbyname($host);

if (!$server) {
	print "Bad Host.\n";
	exit 1;
}

# create the socket, connect to the port
socket(SOCKET,PF_INET,SOCK_STREAM,(getprotobyname('tcp'))[2])
   or die "Can't create a socket $!\n";
my $packed = pack( 'S n a4 x8', &AF_INET, $port, $server );

if (!$packed) {
	print "Bad Host.\n";
	exit 1;
}

connect( SOCKET, $packed) 
       or die "Can't connect to port $port! \n";

my $line;
$line = <SOCKET>;

if ($line) {
	close SOCKET or die "close: $!";
	print "Success.\n";
	exit 0;
} else {
	close SOCKET or die "close: $!";
}
