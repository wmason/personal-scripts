#!/bin/bash
#
##

BASE=`hostname`

MYPATH="`dirname $0`"
CONF=${MYPATH}/port.ini

echo "Name,Host,Port,Status"

echo "<!>Sample_Time,`date +%H:%M:%S`"

for HOST in `grep "^${BASE}" $CONF | grep -v '^#'`
do

	HOST2=`echo $HOST | awk -F: '{print $2}'`
	PORT=`echo $HOST | awk -F: '{print $3}'`
	NAME=`echo $HOST | awk -F: '{print $4}'`

	if [[ -z "$NAME" ]]; then
		NAME=$HOST2
	fi

	$MYPATH/test.pl ${HOST2} ${PORT} 2>/dev/null >/dev/null
	STATUS=$?

	if [[ $STATUS -gt 0 ]]; then
		echo "${NAME},${HOST2},${PORT},down"
	else 
		echo "${NAME},${HOST2},${PORT},up"
	fi
done
	
