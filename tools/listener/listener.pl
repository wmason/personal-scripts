#!/usr/bin/perl
#1/7/2011 12:08 PM
##

($port) = @ARGV;

if (!$port) {

       print "\n Usage: $0 {portnumber}\n";
       print "\n";
       print "  {portnumber}   Can be any valid port number or entry in /etc/services.\n\n";
       exit 0;
}

print "Starting up on port $port.\n";

if (fork()) { exit; }

use IO::Socket;


my $sock = new IO::Socket::INET (
        #LocalHost => 'eXtraTrade',
        LocalPort => $port,
        Proto => 'tcp',
        Listen => 1,
        Reuse => 1,
        );

die "Could not create socket: $!\n" unless $sock;


while (true) {
        my $new_sock = $sock->accept();

        print $new_sock "===================================================================\r\n\r\n";
        print $new_sock "You have successfully connected to a test port.  You will now be\r\n";
        print $new_sock "in a loop where everything sent will be echoed back.\r\n\r\n";
        print $new_sock "===================================================================\r\n\r\n";

        while(<$new_sock>) {
                print $new_sock "RECEIVED: $_\r\n";
        }

}
