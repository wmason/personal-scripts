#!/bin/bash
#
###############################################################################
## mvimg - Search current directory for jpg, png, mp4, mov, gif, cr2 and crw.  Examine filenames
##         and EXIF info for a datestamp (epochi, timestamp or standard).  Ignores file timestamp
##         as that can be very unreliable.
##         Files are placed in "$TARGET/YYYY-MM-DD".  
##         In the event the filename exists on the destination, files are compared and if different, 
##         the filename is appended with a current timestamp.
##
## Requirements:
##     exiftool

TARGET="{directory}"


ORIGFILE="$2"
FILE="`basename \"$2\"`"

if [[ -z "$1" ]]; then
	# ls -- *.mp4 *.jpg *.png *.JPG *.jpeg *.cr2 *.CR2 | sed -e 's/^/.\/mvimg "/' -e 's/$/"/' > file; sh file
	TMPFILE=/tmp/mvimg.$$

	echo "MVIMG=$0 " > $TMPFILE

	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.jpg$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.jpeg$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.png$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.mp4$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.mov$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.gif$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.cr2$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE
	ls --file-type -- 2>/dev/null | grep -v "/"  | grep -i '\.crw$' | sed -e 's/^/\$MVIMG mv "/' -e 's/$/"/' >> $TMPFILE

	COUNT="`wc -l $TMPFILE | awk '{print $1}'`"
	
	if [[ $COUNT -gt 0 ]]; then
		echo "Found $COUNT files to process.  Press Return to start.  Ctrl-C to stop."
		read a
	else 
		echo "Nothing to process here. Exiting."
		exit 0
	fi

	sh $TMPFILE

	exit 0
fi


	
if [[ -n "$FILE" ]]; then
	echo "Looking at $ORIGFILE..."
else
	echo "No file given.  Exiting."
	exit 0
fi

if [[ "$1" != "mv" ]]; then
	exit 1
fi

EXT="`echo "$FILE" | sed -e 's/.*\.//'`"
BASEFILE="`basename "$FILE" "$EXT"`"


DATE=""

################################################################################
# formatted dates

DATE2="`echo $FILE | awk -F_ '{print $2}' | grep '^[12][0-9][0-9][0-9][0-1][0-9][0-3][0-9]' | cut -c 1-8 | sed -e 's/^\(....\)\(..\)\(..\)/\1-\2-\3/'`"

if [[ -n "$DATE2" ]]; then
	if [[ "$DATE2" > "1970-00-00" ]]; then
		echo "Found Date1 in filename: $DATE2"
		DATE=$DATE2
	fi
fi


DATE2="`echo $FILE | awk '{print $1}' | grep '^[12][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]' | cut -c 1-10`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	if [[ "$DATE2" > "1970-00-00" ]]; then
		echo "Found Date2 in Filename: $DATE2"
		DATE=$DATE2
	fi
fi

# android screenshots
DATE2="`echo $FILE | sed -e 's/Screenshot_//' | grep '^[012][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-' | cut -c 1-10`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	echo "Found date4 in filename: $DATE2"
	DATE=$DATE2
fi

# embedded date
DATE2="`echo "$FILE" | grep "[12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]" |  sed -e 's/.*\([12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]\).*/\1/'`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	if [[ "$DATE2" > "1970-00-00" ]]; then
		echo "Found date5 in filename: $DATE2"
		DATE=$DATE2
	fi
fi

DATE2="`echo "$FILE" | grep "[12][0-9][0-9][0-9][01][0-9][0-3][0-9]" |  sed -e 's/.*\([12][0-9][0-9][0-9]\)\([01][0-9]\)\([0-3][0-9]\).*/\1-\2-\3/'`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	if [[ "$DATE2" > "1970-00-00" ]]; then
		echo "Found date6 in filename: $DATE2"
		DATE=$DATE2
	fi
fi

################################################################################
# EPOCH Dates

DATE2="`echo $FILE | sed -e 's/\..*//' -e 's/^.*[_\.]//' | grep '^[012][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$'`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	DATE="`date -d @$( echo "($DATE2 + 500) / 1000" | bc) '+%Y-%m-%d'`"
	echo "Found millisecond1 epoch in filename: $DATE"
fi

DATE2="`echo $FILE | sed -e 's/\..*//' | grep '^[012][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$'`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	DATE="`date -d @$DATE2 '+%Y-%m-%d'`"
	echo "Found epoch1 in filename: $DATE"
fi

# Facebook saves
EPOCH="`echo $FILE | grep '^FB_IMG_' | sed -e 's/FB_IMG_//' | grep '^[012][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' | cut -c 1-10`"
DATE2="`date -d @${EPOCH} '+%Y-%m-%d' 2>/dev/null | grep '^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$'`"

if [[ -n "$DATE2" && -z "$DATE" ]]; then
	echo "Found epoch2 in filename: $DATE2"
	DATE=$DATE2
fi


################################################################################
# EXIF Info

EXIF="`exiftool "${FILE}" 2>/dev/null | grep 'Create Date' | head -1 | sed -e 's/:/-/g' | awk '{print $4}'`"

if [[ -n "$EXIF" ]]; then
	echo "Found EXIF Info, overriding date with: $EXIF"
	DATE=$EXIF
else
	echo "No EXIF date found."
fi

if [[ -n "$DATE" ]]; then
	echo "Using Date: $DATE"

else
	echo "Cannot discern date.  Aborted."
	echo ""
	exit 1
fi

VALID="`echo $DATE | grep '^[12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]$'`"
if [[ -n "${VALID}" ]]; then
	mkdir -p "$TARGET/$DATE"
	STATUS=$?

	if [[ $STATUS -ne 0 ]]; then
		echo "Failed to make the folder.  Aborting."
		echo ""
		exit 2
	fi

	if [[ -e "$TARGET/$DATE/$FILE" ]]; then
		echo "File already exists."
		diff "$ORIGFILE" "$TARGET/$DATE/$FILE"
		STATUS=$?

		if [[ $STATUS -ne 0 ]]; then
			NEWFILE="${BASEFILE}`date "+%s"`.${EXT}"
			echo "Files are different.  Renaming to $NEWFILE"
			mv "$ORIGFILE" "$TARGET/$DATE/$NEWFILE"
			STATUS=$?
		else
			mv "$ORIGFILE" "$TARGET/$DATE"
			STATUS=$?
		fi
	else
		mv "$ORIGFILE" "$TARGET/$DATE"
		STATUS=$?
	fi

	if [[ $STATUS -ne 0 ]]; then
		echo "Failed to move the file [$ORIGFILE].  Aborting."
		echo ""
		exit 3
	fi
	echo "File [$ORIGFILE] moved to [$TARGET/$DATE]."
else
	echo "ERROR: $DATE is not a valid date."
fi

echo ""



